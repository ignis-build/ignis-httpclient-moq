﻿using System.Linq.Expressions;
using Ignis.HttpClient.Moq.Matchers;
using Moq.Protected;

// ReSharper disable once CheckNamespace
namespace Moq;

public static class Request
{
    public static Expression Is(Func<Matcher, Matcher> configure)
    {
        return ItExpr.Is<HttpRequestMessage>(r => r.Is(configure));
    }

    public static Expression Is(Matcher matcher)
    {
        return ItExpr.Is<HttpRequestMessage>(r => r.Is(matcher));
    }
}
