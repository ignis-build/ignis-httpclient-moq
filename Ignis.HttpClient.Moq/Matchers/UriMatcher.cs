﻿namespace Ignis.HttpClient.Moq.Matchers;

/// <summary>
///     Request URI matcher.
/// </summary>
public sealed class UriMatcher : Matcher
{
    private readonly Uri _uri;

    public UriMatcher(string uri) : this(new Uri(uri))
    {
    }

    public UriMatcher(Uri uri)
    {
        _uri = uri;
    }

    public override bool Match(HttpRequestMessage request)
    {
        return request.RequestUri == _uri;
    }
}
