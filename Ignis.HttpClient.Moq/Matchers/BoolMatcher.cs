﻿namespace Ignis.HttpClient.Moq.Matchers;

/// <summary>
///     Always returns true or false matcher.
/// </summary>
public sealed class BoolMatcher : Matcher
{
    private readonly bool _value;

    private BoolMatcher(bool value)
    {
        _value = value;
    }

    /// <summary>
    ///     Always returns true matcher.
    /// </summary>
    public static Matcher True { get; } = new BoolMatcher(true);

    /// <summary>
    ///     Always returns false matcher.
    /// </summary>
    public static Matcher False { get; } = new BoolMatcher(false);

    public override bool Match(HttpRequestMessage request)
    {
        return _value;
    }

    public static Matcher Get(bool value)
    {
        if (value) return True;
        return False;
    }
}
