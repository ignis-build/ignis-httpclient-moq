﻿using Ignis.HttpClient.Moq.Http;

namespace Ignis.HttpClient.Moq.Matchers;

public sealed class HttpMethodMatcher : Matcher
{
    private static readonly Dictionary<string, Lazy<Matcher>> Matchers =
        HttpMethods.All.ToDictionary(method => method.ToString(),
            method => new Lazy<Matcher>(() => new HttpMethodMatcher(method)));

    private readonly HttpMethod _method;

    private HttpMethodMatcher(HttpMethod method)
    {
        _method = method;
    }

    public override bool Match(HttpRequestMessage request)
    {
        return request.Method == _method;
    }

    public static Matcher From(HttpMethod method)
    {
        return Matchers[method.ToString()].Value;
    }

    public static Matcher From(string method)
    {
        return Matchers[method].Value;
    }

    #region http methods.

    public static Matcher Delete => From(HttpMethod.Delete);
    public static Matcher Get => From(HttpMethod.Get);
    public static Matcher Head => From(HttpMethod.Head);
    public static Matcher Options => From(HttpMethod.Options);
    public static Matcher Patch => From("PATCH");
    public static Matcher Put => From(HttpMethod.Put);
    public static Matcher Post => From(HttpMethod.Post);
    public static Matcher Trace => From(HttpMethod.Trace);

    #endregion
}
