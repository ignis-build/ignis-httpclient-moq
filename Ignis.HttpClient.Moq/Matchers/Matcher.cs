﻿using System.Linq.Expressions;

namespace Ignis.HttpClient.Moq.Matchers;

/// <summary>
///     HTTP request matcher abstract class.
/// </summary>
public abstract class Matcher
{
    /// <summary>
    ///     Whether it matches your requirements.
    /// </summary>
    /// <param name="request">HTTP Request.</param>
    /// <returns>Whether it matches your requirements.</returns>
    // ReSharper disable once MemberCanBeProtected.Global
    public abstract bool Match(HttpRequestMessage request);


    public static implicit operator Expression<Func<HttpRequestMessage, bool>>(Matcher matcher)
    {
        return request => matcher.Match(request);
    }
}
