﻿namespace Ignis.HttpClient.Moq.Matchers;

public static class Matchers
{
    public static Matcher And(Matcher left, Matcher right)
    {
        return new AndMatcher(left, right);
    }

    public static Matcher True()
    {
        return BoolMatcher.True;
    }

    // ReSharper disable once UnusedMember.Global
    public static Matcher False()
    {
        return BoolMatcher.False;
    }

    public static Matcher Bool(bool value)
    {
        return BoolMatcher.Get(value);
    }

    #region http method matchers.

    public static Matcher Method(HttpMethod method)
    {
        return HttpMethodMatcher.From(method);
    }
    
    public static Matcher Method(HttpMethod method, string uri)
    {
        return ShorthandMatcher.From(method, uri);
    }

    public static Matcher Method(HttpMethod method, Uri uri)
    {
        return ShorthandMatcher.From(method, uri);
    }

    // ReSharper disable UnusedMember.Global

    public static Matcher Delete()
    {
        return HttpMethodMatcher.Delete;
    }

    public static Matcher Delete(string uri)
    {
        return ShorthandMatcher.Delete(uri);
    }

    public static Matcher Delete(Uri uri)
    {
        return ShorthandMatcher.Delete(uri);
    }

    public static Matcher Get()
    {
        return HttpMethodMatcher.Get;
    }

    public static Matcher Get(string uri)
    {
        return ShorthandMatcher.Get(uri);
    }

    public static Matcher Get(Uri uri)
    {
        return ShorthandMatcher.Get(uri);
    }

    public static Matcher Head()
    {
        return HttpMethodMatcher.Head;
    }

    public static Matcher Head(string uri)
    {
        return ShorthandMatcher.Head(uri);
    }

    public static Matcher Head(Uri uri)
    {
        return ShorthandMatcher.Head(uri);
    }
    
    public static Matcher Options()
    {
        return HttpMethodMatcher.Options;
    }

    public static Matcher Options(string uri)
    {
        return ShorthandMatcher.Options(uri);
    }

    public static Matcher Options(Uri uri)
    {
        return ShorthandMatcher.Options(uri);
    }
    
    public static Matcher Patch()
    {
        return HttpMethodMatcher.Patch;
    }

    public static Matcher Patch(string uri)
    {
        return ShorthandMatcher.Patch(uri);
    }

    public static Matcher Patch(Uri uri)
    {
        return ShorthandMatcher.Patch(uri);
    }

    public static Matcher Put()
    {
        return HttpMethodMatcher.Put;
    }

    public static Matcher Put(string uri)
    {
        return ShorthandMatcher.Put(uri);
    }

    public static Matcher Put(Uri uri)
    {
        return ShorthandMatcher.Put(uri);
    }

    public static Matcher Post()
    {
        return HttpMethodMatcher.Post;
    }

    public static Matcher Post(string uri)
    {
        return ShorthandMatcher.Post(uri);
    }

    public static Matcher Post(Uri uri)
    {
        return ShorthandMatcher.Post(uri);
    }
    
    public static Matcher Trace()
    {
        return HttpMethodMatcher.Trace;
    }

    public static Matcher Trace(string uri)
    {
        return ShorthandMatcher.Trace(uri);
    }

    public static Matcher Trace(Uri uri)
    {
        return ShorthandMatcher.Trace(uri);
    }

    // ReSharper restore UnusedMember.Global

    #endregion

    #region uri matchers.

    public static Matcher Uri(string uri)
    {
        return new UriMatcher(uri);
    }

    // ReSharper disable once UnusedMember.Global
    public static Matcher Uri(Uri uri)
    {
        return new UriMatcher(uri);
    }

    #endregion
}
