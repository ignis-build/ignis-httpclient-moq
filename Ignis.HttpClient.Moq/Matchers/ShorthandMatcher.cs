﻿namespace Ignis.HttpClient.Moq.Matchers;

/// <summary>
///     Shorthand matcher (Ex: "GET https://example.jp")
/// </summary>
public sealed class ShorthandMatcher : Matcher
{
    private readonly AndMatcher _matcher;

    private ShorthandMatcher(HttpMethod method, Uri uri)
    {
        _matcher = new AndMatcher(HttpMethodMatcher.From(method), new UriMatcher(uri));
    }

    private ShorthandMatcher(string method, string uri)
    {
        _matcher = new AndMatcher(HttpMethodMatcher.From(method), new UriMatcher(uri));
    }

    private ShorthandMatcher(string method, Uri uri)
    {
        _matcher = new AndMatcher(HttpMethodMatcher.From(method), new UriMatcher((uri)));
    }

    public override bool Match(HttpRequestMessage request)
    {
        return _matcher.Match(request);
    }

    public static Matcher Delete(string uri)
    {
        return From(HttpMethod.Delete, uri);
    }

    public static Matcher Delete(Uri uri)
    {
        return From(HttpMethod.Delete, uri);
    }

    public static Matcher Get(string uri)
    {
        return From(HttpMethod.Get, uri);
    }

    public static Matcher Get(Uri uri)
    {
        return From(HttpMethod.Get, uri);
    }

    public static Matcher Head(string uri)
    {
        return From(HttpMethod.Head, uri);
    }

    public static Matcher Head(Uri uri)
    {
        return From(HttpMethod.Head, uri);
    }

    public static Matcher Options(string uri)
    {
        return From(HttpMethod.Options, uri);
    }

    public static Matcher Options(Uri uri)
    {
        return From(HttpMethod.Options, uri);
    }

    public static Matcher Patch(string uri)
    {
        return From("PATCH", uri);
    }

    public static Matcher Patch(Uri uri)
    {
        return From("PATCH", uri);
    }

    public static Matcher Put(string uri)
    {
        return From(HttpMethod.Put, uri);
    }

    public static Matcher Put(Uri uri)
    {
        return From(HttpMethod.Put, uri);
    }

    public static Matcher Post(string uri)
    {
        return From(HttpMethod.Post, uri);
    }

    public static Matcher Post(Uri uri)
    {
        return From(HttpMethod.Post, uri);
    }

    public static Matcher Trace(string uri)
    {
        return From(HttpMethod.Trace, uri);
    }

    public static Matcher Trace(Uri uri)
    {
        return From(HttpMethod.Trace, uri);
    }

    public static Matcher From(HttpMethod method, string uri)
    {
        return new ShorthandMatcher(method, new Uri(uri));
    }

    private static Matcher From(string method, string uri)
    {
        return new ShorthandMatcher(method, uri);
    }

    public static Matcher From(HttpMethod method, Uri uri)
    {
        return new ShorthandMatcher(method, uri);
    }

    private static Matcher From(string method, Uri uri)
    {
        return new ShorthandMatcher(method, uri);
    }
}
