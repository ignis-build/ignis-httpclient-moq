﻿using Ignis.HttpClient.Moq.Matchers;

// ReSharper disable once CheckNamespace
namespace System.Net.Http;

public static class HttpRequestMessageExtensions
{
    public static bool Is(this HttpRequestMessage request, Func<Matcher, Matcher> configure)
    {
        var matcher = configure(Matchers.True());
        return request.Is(matcher);
    }

    public static bool Is(this HttpRequestMessage request, Matcher matcher)
    {
        return matcher.Match(request);
    }
}
