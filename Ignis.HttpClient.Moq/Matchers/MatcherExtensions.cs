﻿namespace Ignis.HttpClient.Moq.Matchers;

public static class MatcherExtensions
{
    /// <summary>
    ///     And matcher.
    /// </summary>
    /// <param name="first"></param>
    /// <param name="second"></param>
    /// <returns></returns>
    // ReSharper disable once MemberCanBePrivate.Global
    public static Matcher And(this Matcher first, Matcher second)
    {
        return Matchers.And(first, second);
    }

    public static Matcher Method(this Matcher matcher, HttpMethod method)
    {
        return matcher.And(Matchers.Method(method));
    }
}
