﻿namespace Ignis.HttpClient.Moq.Matchers;

/// <summary>
///     And matcher.
/// </summary>
public sealed class AndMatcher : Matcher
{
    private readonly Matcher _first;
    private readonly Matcher _second;

    public AndMatcher(Matcher first, Matcher second)
    {
        _first = first;
        _second = second;
    }

    public override bool Match(HttpRequestMessage request)
    {
        return _first.Match(request) && _second.Match(request);
    }
}
