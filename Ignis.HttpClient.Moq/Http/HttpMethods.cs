﻿using System.Reflection;

namespace Ignis.HttpClient.Moq.Http;

internal static class HttpMethods
{
    private static readonly Lazy<HttpMethod[]> Lazy = new(() => Create().ToArray());
    public static IEnumerable<HttpMethod> All => Lazy.Value;

    private static IEnumerable<HttpMethod> Create()
    {
        return typeof(HttpMethod)
            .GetProperties(BindingFlags.Static | BindingFlags.Public)
            .Select(property => property.GetValue(null))
            .OfType<HttpMethod>();
    }
}
