using Builds;
using Nuke.Common;
using Nuke.Common.CI;
using Nuke.Common.Execution;

[CheckBuildProjectConfigurations(TimeoutInMilliseconds = 2000)]
[ShutdownDotNetAfterServerBuild]
class Build : NukeBuild, ILint, ITest
{
    public static int Main() => Execute<Build>(x => ((ICompile) x).Compile);
}
