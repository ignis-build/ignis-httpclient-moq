﻿using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;

namespace Builds;

interface ISolution : INukeBuild
{
    [Solution] Solution Solution => TryGetValue(() => Solution);
    AbsolutePath ReportsDirectory => RootDirectory / "reports";
    AbsolutePath CacheDirectory => RootDirectory / ".cache";
}
