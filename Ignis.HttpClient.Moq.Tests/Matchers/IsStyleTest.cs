﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Moq.Protected;
using static Ignis.HttpClient.Moq.Matchers.Matchers;

namespace Ignis.HttpClient.Moq.Matchers;

public sealed class IsStyleTest : IDisposable
{
    private readonly System.Net.Http.HttpClient _client;
    private readonly Mock<HttpMessageHandler> _mock;

    public IsStyleTest()
    {
        _mock = new Mock<HttpMessageHandler>();
        _client = _mock.CreateHttpClient();
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Fact]
    public async Task TestIs()
    {
        _mock.SetupSendAsync(ItExpr.Is<HttpRequestMessage>(r => r.Is(matcher => matcher.Method(HttpMethod.Get))))
            .Returns(Responses.Ok);

        await _client.GetAsync("/");
    }

    [Fact]
    public async Task TestIsUseMatchers()
    {
        _mock.SetupSendAsync(ItExpr.Is<HttpRequestMessage>(r => r.Is(Method(HttpMethod.Get))))
            .Returns(Responses.Ok);

        await _client.GetAsync("/");
    }
}
