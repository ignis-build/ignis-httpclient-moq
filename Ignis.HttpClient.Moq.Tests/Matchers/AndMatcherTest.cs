﻿using System.Net.Http;
using PowerAssert;

namespace Ignis.HttpClient.Moq.Matchers;

public sealed class AndMatcherTest
{
    [Fact]
    public void TestReturnsTrue()
    {
        var target = Matchers.And(BoolMatcher.True, BoolMatcher.True);

        PAssert.IsTrue(() => target.Match(new HttpRequestMessage()));
    }

    [Theory]
    [InlineData(true, false)]
    [InlineData(false, true)]
    [InlineData(false, false)]
    public void TestReturnsFalse(bool first, bool second)
    {
        var target = Matchers.And(Matchers.Bool(first), Matchers.Bool(second));

        PAssert.IsTrue(() => !target.Match(new HttpRequestMessage()));
    }
}
