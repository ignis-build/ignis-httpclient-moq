﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using static Ignis.HttpClient.Moq.Matchers.Matchers;

namespace Ignis.HttpClient.Moq.Matchers;

public sealed class ShorthandMatcherTest : IDisposable
{
    private readonly System.Net.Http.HttpClient _client;
    private readonly Mock<HttpMessageHandler> _mock;

    public ShorthandMatcherTest()
    {
        _mock = new Mock<HttpMessageHandler>();
        _client = _mock.CreateHttpClient();
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Theory]
    [MemberData(nameof(HttpMethods))]
    public async Task TestMethodWithUri(HttpMethod method)
    {
        _mock.SetupSendAsync(Request.Is(Method(method, new Uri("https://example.jp/abc"))))
            .Returns(Responses.Ok);

        await SendAsync(method, "https://example.jp/abc");
    }

    [Theory]
    [MemberData(nameof(HttpMethods))]
    public async Task TestMethodWithStringUri(HttpMethod method)
    {
        _mock.SetupSendAsync(Request.Is(Method(method, "https://example.jp/abc")))
            .Returns(Responses.Ok);

        await SendAsync(method, "https://example.jp/abc");
    }

    [Theory]
    [MemberData(nameof(HttpMethods))]
    public async Task TestDirectMethodWithUri(HttpMethod method)
    {
        _mock.SetupSendAsync(Request.Is(HttpMethodMatchers.HttpMethod(method, new Uri("https://example.jp/abc"))))
            .Returns(Responses.Ok);

        await SendAsync(method, "https://example.jp/abc");
    }

    [Theory]
    [MemberData(nameof(HttpMethods))]
    public async Task TestDirectMethodWithStringUri(HttpMethod method)
    {
        _mock.SetupSendAsync(Request.Is(HttpMethodMatchers.HttpMethod(method, "https://example.jp/abc")))
            .Returns(Responses.Ok);

        await SendAsync(method, "https://example.jp/abc");
    }

    private async Task SendAsync(HttpMethod method, string uri)
    {
        await _client.SendAsync(new HttpRequestMessage
        {
            Method = method,
            RequestUri = new Uri(uri)
        });
    }

    private static IEnumerable<object> HttpMethods()
    {
        return Http.HttpMethods.All
            .Select(method => new object[] {method});
    }
}
