﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using static Ignis.HttpClient.Moq.Matchers.Matchers;

namespace Ignis.HttpClient.Moq.Matchers;

public sealed class HttpMethodMatcherTest : IDisposable
{
    private readonly System.Net.Http.HttpClient _client;
    private readonly Mock<HttpMessageHandler> _mock;

    public HttpMethodMatcherTest()
    {
        _mock = new Mock<HttpMessageHandler>();
        _client = _mock.CreateHttpClient();
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Theory]
    [MemberData(nameof(HttpMethods))]
    public async Task TestMethod(HttpMethod method)
    {
        _mock.SetupSendAsync(Request.Is(Method(method)))
            .Returns(Responses.Ok);

        await _client.SendAsync(new HttpRequestMessage {Method = method});
    }

    [Theory]
    [MemberData(nameof(HttpMethods))]
    public async Task TestDirectMethod(HttpMethod method)
    {
        _mock.SetupSendAsync(Request.Is(HttpMethodMatchers.HttpMethod(method)))
            .Returns(Responses.Ok);

        await _client.SendAsync(new HttpRequestMessage {Method = method});
    }

    private static IEnumerable<object> HttpMethods()
    {
        return Http.HttpMethods.All
            .Select(method => new object[] {method});
    }
}
