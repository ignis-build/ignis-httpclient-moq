﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Reflection;

namespace Ignis.HttpClient.Moq.Matchers;

internal static class HttpMethodMatchers
{
    public static Matcher HttpMethod(HttpMethod method)
    {
        var func = typeof(Matchers).GetMethod(method.ToString(),
            BindingFlags.Public | BindingFlags.Static | BindingFlags.IgnoreCase,
            Array.Empty<Type>());
        Debug.Assert(func != null, nameof(func) + " != null");
        return (Matcher) func.Invoke(null, Array.Empty<object>())!;
    }

    public static Matcher HttpMethod(HttpMethod method, string uri)
    {
        var func = typeof(Matchers).GetMethod(method.ToString(),
            BindingFlags.Public | BindingFlags.Static | BindingFlags.IgnoreCase,
            new[] {typeof(string)});
        Debug.Assert(func != null, nameof(func) + " != null");
        return (Matcher) func.Invoke(null, new object[] {uri})!;
    }

    public static Matcher HttpMethod(HttpMethod method, Uri uri)
    {
        var func = typeof(Matchers).GetMethod(method.ToString(),
            BindingFlags.Public | BindingFlags.Static | BindingFlags.IgnoreCase,
            new[] {typeof(Uri)});
        Debug.Assert(func != null, nameof(func) + " != null");
        return (Matcher) func.Invoke(null, new object[] {uri})!;
    }
}
