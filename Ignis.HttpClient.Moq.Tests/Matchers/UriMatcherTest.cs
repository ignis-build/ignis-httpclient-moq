﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using PowerAssert;
using static Ignis.HttpClient.Moq.Matchers.Matchers;

namespace Ignis.HttpClient.Moq.Matchers;

public sealed class UriMatcherTest : IDisposable
{
    private readonly System.Net.Http.HttpClient _client;
    private readonly Mock<HttpMessageHandler> _mock;

    public UriMatcherTest()
    {
        _mock = new Mock<HttpMessageHandler>();
        _client = _mock.CreateHttpClient();
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Fact]
    public async Task TestMatch()
    {
        _mock.SetupSendAsync(Request.Is(Uri("https://example.jp/abc")))
            .Returns(Responses.Ok);

        await _client.GetAsync("/abc");
    }

    [Fact]
    public async Task TestNotMatch()
    {
        _mock.SetupSendAsync(Request.Is(Uri("https://example.jp/abc")))
            .Returns(Responses.Ok);

        await PAssert.Throws<Exception>(() => _client.GetAsync("/"));
    }
}
