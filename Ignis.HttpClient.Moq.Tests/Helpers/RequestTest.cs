﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Ignis.HttpClient.Moq.Matchers;
using Moq.Protected;
using static Ignis.HttpClient.Moq.Matchers.Matchers;

namespace Ignis.HttpClient.Moq.Helpers;

public sealed class RequestTest : IDisposable
{
    private readonly System.Net.Http.HttpClient _client;
    private readonly Mock<HttpMessageHandler> _mock;

    public RequestTest()
    {
        _mock = new Mock<HttpMessageHandler>();
        _client = _mock.CreateHttpClient();
    }

    public void Dispose()
    {
        _client.Dispose();
    }

    [Fact]
    public async Task TestIsConfigure()
    {
        _mock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync",
                Request.Is(r => r.Method(HttpMethod.Get)),
                ItExpr.IsAny<CancellationToken>())
            .Returns(Task.FromResult(new HttpResponseMessage()));

        await _client.GetAsync("/");
    }

    [Fact]
    public void TestIsMatcher()
    {
        _mock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync",
                Request.Is(Method(HttpMethod.Get)),
                ItExpr.IsAny<CancellationToken>())
            .Returns(Task.FromResult(new HttpResponseMessage()));
    }
}
