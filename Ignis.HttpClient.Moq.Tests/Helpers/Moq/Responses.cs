﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

// ReSharper disable once CheckNamespace
namespace Moq;

internal static class Responses
{
    public static Task<HttpResponseMessage> Ok()
    {
        return Task.FromResult(new HttpResponseMessage
        {
            StatusCode = HttpStatusCode.OK
        });
    }
}
