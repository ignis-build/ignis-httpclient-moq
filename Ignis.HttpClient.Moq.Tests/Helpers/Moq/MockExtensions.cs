﻿using System;
using System.Linq.Expressions;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq.Language.Flow;
using Moq.Protected;

// ReSharper disable once CheckNamespace
namespace Moq;

internal static class MockExtensions
{
    public static HttpClient CreateHttpClient(this Mock<HttpMessageHandler> mock)
    {
        return new HttpClient(mock.Object)
        {
            BaseAddress = new Uri("https://example.jp")
        };
    }

    public static ISetup<HttpMessageHandler, Task<HttpResponseMessage>> SetupSendAsync(
        this Mock<HttpMessageHandler> mock, Expression request)
    {
        return mock.Protected()
            .Setup<Task<HttpResponseMessage>>("SendAsync",
                request,
                ItExpr.IsAny<CancellationToken>());
    }
}
